Source: tuxonice-userui
Priority: optional
Section: contrib/misc
Maintainer: Julien Muchembled <jm@jmuchemb.eu>
Build-Depends: debhelper (>= 9~),
               libmng-dev (>= 1.0.5),
               libfreetype6-dev,
               libpng-dev,
Standards-Version: 4.1.4
Homepage: https://gitlab.com/nigelcunningham/Tuxonice-Userui
Vcs-Git: https://salsa.debian.org/debian/tuxonice-userui.git
Vcs-Browser: https://salsa.debian.org/debian/tuxonice-userui

Package: tuxonice-userui
Architecture: i386 ia64 powerpc ppc64 amd64
Depends: ${shlibs:Depends}, ${misc:Depends}
Breaks: systemd (<= 208-6)
Recommends: initramfs-tools, pm-utils | systemd-sysv
Description: user-space interfaces for TuxOnIce
 TuxOnIce is an alternative implementation to suspend a Linux machine
 by writing its state to memory for later resuming. A kernel patch for
 TuxOnIce is available separately.
 .
 This package contains several user interfaces which improve the user's
 experience during the suspend and resume processes. It also adds TuxOnIce
 support to initramfs and provides a way to configure TuxOnIce settings.
